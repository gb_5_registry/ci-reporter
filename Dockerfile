FROM python:3.8.9-alpine3.12
LABEL maintainer="v.dovnar@city-mobil.ru"

RUN addgroup -S ibuser && adduser -S -G ibuser ibuser
WORKDIR /ci-reporer
COPY . .
RUN pip3 install -r requirements.txt
RUN chmod +x ./ci-reporter.py;  ln ./ci-reporter.py /usr/bin/ci-reporter
USER ibuser
ENTRYPOINT []

