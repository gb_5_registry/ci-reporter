#!/usr/bin/env python3

import requests
from os import environ, path
from sys import argv

def report(sast, report):
    namespace = environ['CI_PROJECT_NAMESPACE']
    project = environ['CI_PROJECT_NAME']
    chat_id = environ['chat_id']
    telegram_token = environ['TELEGRAM_BOT_TOKEN']
    msg = f'Выполнено сканирование для {namespace}/{project}\n' \
          f'SAST: {sast}'

    url_prefix = 'http://94.130.59.30:13200'
    url_prefix = 'https://api.telegram.org'
    telegram_url = f'{url_prefix}/bot{telegram_token}'

    headers = {'Content-Type': 'application/json'}
    prepare_msg = {"chat_id": chat_id, "text": msg, "disable_notification": True}
    prepare_err_msg = {"chat_id": chat_id, "text": f'Возникла ошибка с отправкой файла\nSAST:{sast}'}

    proxies = {
        'http': 'http://127.0.0.1:8080',
        'https': 'http://127.0.0.1:8080',
    }
    proxies = None


    if path.exists(report):
        # send message
        a = requests.post(f'{telegram_url}/sendMessage', headers=headers, json=prepare_msg, proxies=proxies)
        #upload file
        r = requests.post(f'{telegram_url}/sendDocument?chat_id={chat_id}', files={'document': open(report, 'rb')},proxies=proxies)
        print(r.json())
    else:
        r = requests.post(f'{telegram_url}/sendMessage', headers=headers, json=prepare_err_msg, proxies=proxies)

if __name__ == '__main__':
    if len(argv) > 1 and argv[1] == '--help':
        print('[x] ci_reporter\nFor control use env variable')
        exit(0)

    if environ.get('SAST_GitLeaks_Enabled', '') == 'True':
        report_path = environ['SAST_GitLeaks_Report_Location']
        report('GitLeaks', report_path)
    if environ.get('SAST_TruffleHog_Enabled', '') == 'True':
        report_path = environ['SAST_TruffleHog_Report_Location']
        report('TruffleHog', report_path)
    
    if environ.get('SAST_Gosec_Enabled','') == 'True':
        report_path = environ['SAST_Gosec_Report_Location']
        report('Gosec', report_path)

